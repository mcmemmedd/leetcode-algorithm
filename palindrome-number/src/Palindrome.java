import java.util.Scanner;

public class Palindrome {
    public static void main(String[] args) {
        System.out.println(isPalindrome(121)); //121, 242, 363, 484
    }


    public static boolean isPalindrome(int x) {
        String s = String.valueOf(x);

        int i = 0;
        int j = s.length() - 1;

        while(i <= j)
        {
            if(s.charAt(i) != s.charAt(j))
                return false;
            i++;
            j--;
        }

        return true;
    }
}