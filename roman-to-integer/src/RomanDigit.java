public class RomanDigit {
    public static void main(String[] args) {
        calculateRomanDigit("MCD");
    }

    public static int calculateRomanDigit(final String inputRoman) {
        int answer = 0;
        int number = 0;

        for (int a = inputRoman.length() - 1; a >= 0; a--) {
            switch (inputRoman.charAt(a)) {
                case 'I' -> number = 1;
                case 'V' -> number = 5;
                case 'X' -> number = 10;
                case 'L' -> number = 50;
                case 'C' -> number = 100;
                case 'D' -> number = 500;
                case 'M' -> number = 1000;
            }

            if (2 * number < answer) {
                answer = answer - number;

            } else {
                answer = answer + number;

            }
        }
        System.out.println("Result: " + answer);
        return answer;
    }
}