package n_two_sum;

import java.util.HashMap;
import java.util.Map;

public class SumMapClass {

    public static void main(String[] args) {
        final int[] randomArray = new int[]{2, 7, 11, 15};
        twoSumWithMap(randomArray, 9);
    }

    /**
     * Create two sum logic with HashMap -> Big(O) = O(n)
     */
    public static int[] twoSumWithMap(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();

        int arraySize = nums.length;
        for (int a = 0; a < arraySize; a++) {

            int temp = target - nums[a];

            if (map.containsKey(temp)) {
                System.out.println("a= " + a + " : " + "b= " + map.get(temp));
                return new int[]{a, map.get(temp)};
            }

            map.put(nums[a], a);
        }
        return null;
    }
}
