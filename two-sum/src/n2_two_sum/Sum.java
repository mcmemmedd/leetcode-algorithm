package n2_two_sum;

public class Sum {
    public static void main(String[] args) {
        int[] randomArray = {3, 6, 2, 8, 9, 1, 4, 0};
        calculateSum(randomArray, 13);
    }

    /**
     * two sum ->  Big(O) n^2
     * input digit = array digits index cemi
     * mumkun butun variantlar
     */

    public static int[] calculateSum(int[] nums, int target) {
        final int[] innerArray = new int[2];
        int arraySize = nums.length;
        for (int a = 0; a < arraySize; a++) {
            for (int b = a + 1; b < arraySize; b++) {
                if (nums[a] + nums[b] == target) {
                    System.out.println("a= " + a + ": " + "b= " + b);
                    innerArray[0] = a;
                    innerArray[1] = b;
                    break;
                }
            }
        }
        return innerArray;
    }
}