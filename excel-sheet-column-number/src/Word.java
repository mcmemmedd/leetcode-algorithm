public class Word {
    public static void main(String[] args) {
        calculateAlphabet("ADA");
    }

    /**
     * AB = (1*26)+2=28
     * ADA = (((1*26)+4)26) + 1= 581
     * umumi word sayi = 26
     */
    private static int calculateAlphabet(final String inputAlphabet) {
        if (inputAlphabet == null) {
            return -1;
        }

        int digit = 0;
        for (char wordChar : inputAlphabet.toUpperCase().toCharArray()) {
            digit *= 26;
            digit += wordChar - 'A' + 1;
        }
        System.out.println(digit);
        return digit;
    }
}