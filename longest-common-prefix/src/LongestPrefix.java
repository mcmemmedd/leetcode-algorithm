public class LongestPrefix {
    public static void main(String[] args) {
        final String[] textArray = new String[]{"alma", "almas", "almali"};
        longestCommonPrefix(textArray);
    }

    /**
     * {"alma", alsam", "almali"} -> common prefix: (al)
     */
    public static String longestCommonPrefix(String[] strings) {
        if (strings == null || strings.length == 0) {
            return "This is the array null";
        }

        String prefix = strings[0];

        for (int a = 1; a < strings.length; a++) {
            while (!strings[a].startsWith(prefix)) {

                prefix = prefix.substring(0, prefix.length() - 1);

                if (prefix.isEmpty()) {
                    return "This is prefix empty";
                }
            }
        }

        System.out.println("Common prefix= " + prefix);
        return prefix;
    }

}