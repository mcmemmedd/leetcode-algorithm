import java.util.Arrays;

public class Duplicate {
    public static void main(String[] args) {
        final int[] randomArray = new int[]{1, 2, 35, 1};
        System.out.println(isDuplicate(randomArray));
    }

    private static boolean isDuplicate(int[] arrayNum) {
        Arrays.sort(arrayNum);
        int size = arrayNum.length - 1;

        for (int a = 0; a < size; a++) {
            if (arrayNum[a] == arrayNum[a + 1]) {
                return true;
            }
        }
        return false;
    }
}
